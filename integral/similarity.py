from abc import ABCMeta, abstractmethod
from collections import OrderedDict
import csv
import logging
import os
import errno
import tempfile
from gensim import corpora, models, similarities
import itertools
import numpy as np
import pandas as pd

FORMAT = '%(asctime)-15s %(levelname)s\t%(filename)s:%(lineno)d\t%(funcName)s %(message)s'
logging.basicConfig(format=FORMAT, level=logging.INFO)

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def chunk(iterable, chunk_size):
    """Generate sequences of `chunk_size` elements from `iterable`."""
    iterable = iter(iterable)
    while True:
        cur_chunk = []
        try:
            for _ in range(chunk_size):
                cur_chunk.append(iterable.next())
            yield cur_chunk
        except StopIteration:
            if cur_chunk:
                yield cur_chunk
            break


class Model(object):
    __metaclass__ = ABCMeta

    def __init__(self, input_filename):
        self.input_filename = input_filename

    @abstractmethod
    def create_model(self):
        pass

    def load_visits_from_file(self, filename):
        with open(filename, 'r') as tsvfile:
            for x in self.load_visits(tsvfile):
                yield x

    @staticmethod
    def load_visits(tsvfile):
        reader = csv.reader(tsvfile, delimiter='\t')
        cur_ip = None
        history = []
        for row in reader:
            ip, url, visits = row[0], row[1], int(row[2])
            if ip != cur_ip:
                if cur_ip:
                    yield (cur_ip, history)
                cur_ip = ip
                history = []
            history += [url] * visits
        yield (cur_ip, history)


class LdaModel(Model):
    def __init__(self, lda_filename):
        super(LdaModel, self).__init__(lda_filename)
        self.lda_filename = lda_filename

        self.dictionary = None
        self.model = None
        self.create_model()

    def create_model(self):
        logging.debug('Building LDA model from scratch, filename %s' % self.lda_filename)
        self.dictionary = corpora.Dictionary((doc[1] for doc in self.load_visits_from_file(self.lda_filename)))
        self.dictionary.filter_extremes(no_below=1, no_above=0.5)

        bow_corpus = [self.dictionary.doc2bow(doc[1]) for doc in self.load_visits_from_file(self.lda_filename)]
        self.model = models.LdaMulticore(bow_corpus, id2word=self.dictionary, num_topics=30, passes=10, workers=2)
        logging.info('LDA model trained')
        return self


class SimilarityIndex(Model):
    def __init__(self, lda_model, crooks_filename):
        super(SimilarityIndex, self).__init__(input_filename=crooks_filename)
        self.lda_model = lda_model
        self.crooks_filename = crooks_filename

        self.dictionary = lda_model.dictionary
        self.model = lda_model.model
        self.index = None
        logging.debug('Create similarity index for %s' % os.path.basename(self.crooks_filename))
        self.create_model()

    def create_model(self):
        bow_corpus = [self.dictionary.doc2bow(doc[1]) for doc in self.load_visits_from_file(self.crooks_filename)]
        self.index = similarities.MatrixSimilarity(self.model[bow_corpus], num_features=self.model.num_topics)

        logging.info('Similarity index for %s created' % self.crooks_filename)
        return self

    def calculate_similarities(self, items, n_best=30):
        return np.transpose(np.vstack((np.array([item[0] for item in items]),
                                       np.mean(
                                           np.partition(self.index[self.model[
                                               [self.dictionary.doc2bow(item[1]) for item in items]]],
                                                        -n_best, axis=1)[:, -n_best:],
                                           axis=1)
                                       )))
        # return np.transpose(np.vstack((np.array([item[0] for item in items]),
        #                                np.mean(self.index[self.model[[self.dictionary.doc2bow(item[1]) for item in items]]],
        #                                        axis=1)
        #                                )))

    def visits_to_similarities(self, fin, fout):
        chunk_size = 500
        for i, docs_chunk in enumerate(chunk(self.load_visits(fin), chunk_size)):
            processed = self.process_chunk(docs_chunk)
            fout.writelines([processed, '\n'])
            fout.flush()
            logging.info('Chunk %s' % i)
        logging.info('Finished')

    def process_chunk(self, docs_chunk):
        return '\n'.join(map(lambda cols: '\t'.join(cols), self.calculate_similarities(docs_chunk).tolist()))

class Graph(Model):
    def __init__(self, input_filename):
        super(Graph, self).__init__(input_filename)
        self.visits_filename = input_filename
        self.graph = None
        self.create_model()

    def _load_graph(self, tsvfile):
        with open(tsvfile, 'r') as fin:
            reader = csv.reader(fin, delimiter='\t')
            for row in reader:
                yield row[0], row[1], int(row[2])

    def create_model(self):
        self.graph = list(self._load_graph(self.visits_filename))
        users, hosts, _ = map(set, zip(*self.graph))

    def dump(self, outfile):
        users, hosts, _ = map(set, zip(*self.graph))
        reverse_index = OrderedDict(((name, i+1) for i, name in enumerate(users | hosts)))
        with open(outfile, 'w') as f:
            f.write('*Vertices %s\n' % len(reverse_index))
            f.write('\n'.join(
                ('%s "%s"' % (i, name) for (name, i) in reverse_index.iteritems())
            ) + '\n')
            f.write('*arcs\n')
            f.write('\n'.join(
                ('%s %s %s' % (reverse_index[user], reverse_index[host], weight) for (user, host, weight) in self.graph)
            ))

def to_graph(visits_file):
    Graph(visits_file).dump('/home/ask/graph_bad.net')


def rank_suspicious_clients(visits_file, crooks_file, suspects_file, suspects_file_out):
    lda_model = LdaModel(visits_file)
    crooks_index = SimilarityIndex(lda_model=lda_model, crooks_filename=crooks_file)

    with open(suspects_file, 'r') as fin, tempfile.NamedTemporaryFile() as fout:
        crooks_index.visits_to_similarities(fin, fout)
        pd.read_csv(fout.name, sep='\t', names='ip,suspiciousness'.split(',')).sort('suspiciousness', ascending=False). \
            to_csv(suspects_file_out, sep='\t', index=False, header=True)


if __name__ == "__main__":
    working_dir = '/home/ask/devel/integral/data'
    visits_file = os.path.join(working_dir, 'all_traffic.tsv')
    crooks_file = os.path.join(working_dir, 'bad_traffic.tsv')
    suspects_file = os.path.join(working_dir, 'suspects.tsv')
    suspects_ranked_file = os.path.join(working_dir, 'suspects_ranked.tsv')
    # to_graph(crooks_file)
    rank_suspicious_clients(visits_file=visits_file, crooks_file=crooks_file, suspects_file=suspects_file, suspects_file_out=suspects_ranked_file)
