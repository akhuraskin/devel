__author__ = 'ask'
from collections import deque


class Item(object):
    def __init__(self, key, value):
        super(Item, self).__init__()
        self.k = key
        self.value = value

class Stats(object):
    def add(self, v):
        pass
    def remove(self, v):
        pass
    def get(self):
        pass

class Mean(Stats):
    def __init__(self):
        super(Mean, self).__init__()
        self.sum = 0.
        self.num = 0

    def add(self, v):
        self.sum += v
        self.num += 1

    def remove(self, v):
        self.sum -= v
        self.num -= 1

    def get(self):
        return self.sum/self.num if self.num else 0.


class Observations(object):
    def __init__(self, interval):
        super(Observations, self).__init__()
        self.interval = interval
        self.c = deque()
        self.stats = [Mean()]

    def _get_current_time(self):
        return 1    # todo

    def prune(self):
        while self.c and self._get_current_time()-self.c[0].k > self.interval:
            item = self.c.popleft()
            map(lambda x: x.remove(item.v), self.stats)

    def r(self, v):
        self.c.append(Item(self._get_current_time(), v))
        self.prune()

    def m(self):
        self.prune()
        return map(lambda x: x.get(), self.stats)
