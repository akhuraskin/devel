from itertools import izip
import sys

__author__ = 'ask'

# todo
def decode(s):
    s = sorted(s)
    l = []
    print s
    for c1, c2 in izip(s[0::2], s[1::2]):
        l.append(c1)
    return ''.join(l)


if __name__ == '__main__':
    s = sys.stdin.readline().strip()
    print decode(s)