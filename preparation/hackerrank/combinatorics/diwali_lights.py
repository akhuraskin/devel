__author__ = 'ask'

modulo = 100000


def diwali(n):
    result = 1
    step = 10
    while n > step:
        result = (result * (1 << step)) % modulo
        n -= step
    result = (result * (1 << n)) % modulo

    return (result - 1) % modulo


if __name__ == '__main__':
    N = int(raw_input())
    for i in xrange(N):
        print diwali(int(raw_input()))