__author__ = 'ask'

M = 1000000007

def trace(m, n):
    m, n = (min(m, n), max(m, n))
    a = [1]*n
    for i in xrange(1, m):
        a = a[1:]
        a[0] += a[0]
        a[0] %= M
        for j in xrange(1, n-i):
            a[j] += a[j-1]
            a[j] %= M
    return a[-1]

N = input()
for _ in xrange(N):
    k, n = map(int, raw_input().split(' '))
    print str(trace(n, k))

# print trace(5,5)