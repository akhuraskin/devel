import sys

__author__ = 'ask'

modulo = 1000000000 + 7


def factorial(n, cache=[1, 1] + [None] * 999):
    if cache[n] is None:
        cache[n] = (factorial(n - 1) * n) % modulo
    return cache[n]


def nCk(n, k):
    f = factorial
    return int(f(n) / f(k) / f(n - k)) % modulo


def nCr(n, r, cache=[[None] * 1001] * 1001):
    if r == 0 or r == n:
        return 1
    if cache[n][r] is None:
        cache[n][r] = (nCr(n - 1, r - 1) % modulo + nCr(n - 1, r) % modulo) % modulo
    return cache[n][r]


def number_of_permutations(n, m):
    if m == 0:
        return 0
    # if m == 1:
    #     return 1
    return nCr(m + n - 1, n)


if __name__ == '__main__':
    # for i in range(11)[1:]:
    print number_of_permutations(2, 4)
    # N = int(raw_input())
    # for i in xrange(N):
    # n, m = map(int, sys.stdin.readline().strip().split(' '))
    # print number_of_permutations(n, m)