__author__ = 'ask'


def largest_rectangle(a):
    width = [1]*len(a)
    shorter_than_cur = []

    n = len(a)
    for i in xrange(n):
        while shorter_than_cur and a[shorter_than_cur[-1]] >= a[i]:
            shorter_than_cur.pop()
        if shorter_than_cur:
            width[i] += i-shorter_than_cur[-1]-1
        else:
            width[i] += i
        shorter_than_cur.append(i)

    shorter_than_cur = []
    for i in xrange(n-1, -1, -1):
        while shorter_than_cur and a[shorter_than_cur[-1]] >= a[i]:
            shorter_than_cur.pop()
        if shorter_than_cur:
            width[i] += shorter_than_cur[-1]-i-1
        else:
            width[i] += n-i-1
        shorter_than_cur.append(i)

    return max(map(lambda x: x[0]*x[1], zip(a, width)))



N = input()
print largest_rectangle(map(int, raw_input().split(' ')))

# print largest_rectangle([1, 2, 3, 2, 2, 1])