import bisect
import sys

__author__ = 'ask'

def find_the_rightmost_disorder(l):
    for i, (left, right) in enumerate(zip(l[-2::-1], l[-1::-1])):
        if left < right:
            return len(l)-2-i
    return -1


def increment(s):
    l = list(s)
    rightmost_disorder = find_the_rightmost_disorder(l)
    if rightmost_disorder > -1:
        boundary = l[rightmost_disorder]
        right_part = sorted(l[rightmost_disorder+1:])
        closest_to_boundary_index = bisect.bisect_right(right_part, boundary)
        l[rightmost_disorder], right_part[closest_to_boundary_index] = right_part[closest_to_boundary_index], l[rightmost_disorder]

        l[rightmost_disorder+1:] = sorted(right_part)
        return ''.join(l)
    return 'no answer'

if __name__ == '__main__':
    # print increment1('10132')
    N = int(raw_input())
    for i in xrange(N):
        s = sys.stdin.readline().strip()
        print increment(s)