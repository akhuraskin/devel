__author__ = 'ask'


def count(l):
    c = [0]*100
    for x in l:
        c[x] += 1
    return c

def replicate(counts):
    return reduce(lambda x,y: x+y, ([i]*count for i, count in enumerate(counts)))

if __name__ == '__main__':
    N = int(raw_input())
    print ' '.join(map(str, replicate(count(map(int, raw_input().split(' '))))))