__author__ = 'ask'
import sys

class UnionFind(object):
    def __init__(self, n):
        self.id = range(n)
        self.c = n

    def union(self, p, q):
        if not self.connected(p, q):
            gp = self.id[p]
            gq = self.id[q]
            for i in xrange(len(self.id)):
                if self.id[i] == gp:
                    self.id[i] = gq
            self.c -= 1

    def find(self, p):
        return self.id[p]

    def connected(self, p, q):
        return self.id[p] == self.id[q]

    def count(self):
        return self.c

class QuickUnion(object):
    def __init__(self, n):
        self.id = range(n)
        self.c = n

    def union(self, p, q):
        root_p = self.find(p)
        root_q = self.find(q)
        if root_p != root_q:
            self.id[root_q] = self.id[root_p]
            self.c -= 1

    def find(self, p):
        while self.id[p] != p:
            p = self.id[p]
        return p

    def connected(self, p, q):
        return self.find(p) == self.find(q)

    def count(self):
        return self.c


class WeightedQuickUnion(object):
    def __init__(self, n):
        self.id = range(n)
        self.sz = [1]*n
        self.c = n

    def union(self, p, q):
        root_p = self.find(p)
        root_q = self.find(q)
        if root_p != root_q:
            smaller, larger = (root_p, root_q) if self.sz[root_p] < self.sz[root_q] else (root_q, root_p)
            self.id[smaller] = self.id[larger]
            self.sz[larger] += self.sz[smaller]
            self.c -= 1

    def find(self, p):
        while self.id[p] != p:
            p = self.id[p]
        return p

    def connected(self, p, q):
        return self.find(p) == self.find(q)

    def count(self):
        return self.c

N = input()
uf = WeightedQuickUnion(N)
# uf = QuickUnion(N)
line = sys.stdin.readline()
while line:
    p, q = map(int, line.strip().split(' '))
    uf.union(p, q)
    print p, q
    print uf.count()
    line = sys.stdin.readline()
print uf.count()
# print '\t'.join((str(i) for i in xrange(N)))
# print ' \t'.join(map(str, uf.id))