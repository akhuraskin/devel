import sys

__author__ = 'ask'


class UnionFind(object):
    def __init__(self, n):
        self.id = range(n)
        self.sz = [1] * n
        self.c = n

    def union(self, p, q):
        root_p = self.find(p)
        root_q = self.find(q)
        if root_p != root_q:
            smaller, larger = (root_p, root_q) if self.sz[root_p] < self.sz[root_q] else (root_q, root_p)
            self.id[smaller] = self.id[larger]
            self.sz[larger] += self.sz[smaller]
            self.c -= 1

    def find(self, p):
        while self.id[p] != p:
            p = self.id[p]
        return p

    def connected(self, p, q):
        return self.find(p) == self.find(q)

    def count(self):
        return self.c

    def flatten(self):
        for i in xrange(len(self.id)):
            self.id[i] = self.find(i)

    def components_size(self):
        self.flatten()
        sizes = {}
        for c_id in self.id:
            sizes[c_id] = self.sz[c_id]
        return sorted(sizes.values(), reverse=True)


class Basins(object):
    def __init__(self, fin):
        self.fin = fin
        self.N = int(self.fin.readline().strip())
        self.height = []
        self.load()
        self.uf = UnionFind(self.N * self.N)

    def load(self):
        for _ in xrange(self.N):
            line = self.fin.readline()
            self.height += map(int, line.strip().split(' '))

    def valid_z(self, z):
        return 0 <= z < self.N * self.N

    def valid_xy(self, x, y):
        return 0 <= x < self.N and 0 <= y < self.N

    def neighbors(self, z):
        if not self.valid_z(z):
            return []
        x, y = self.xy(z)
        return (self.z(x, y) for x, y in
                ((x, y) for (x, y) in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)] if self.valid_xy(x, y)))

    def find_basins(self):
        for z, h in enumerate(self.height):
            heights_lower = [(zn, self.height[zn]) for zn in self.neighbors(z) if self.height[zn] < h]
            if heights_lower:
                flows_to = min(heights_lower, key=lambda t: t[1])[0]
                self.uf.union(z, flows_to)
                # print self.xy(z), self.xy(flows_to)

    def z(self, x, y):
        return x * self.N + y

    def xy(self, z):
        return z / self.N, z % self.N

    def dump(self):
        return '\n'.join(map(str, [self.uf.id[i * self.N: (i + 1) * self.N] for i in xrange(self.N)]))


def main():
    with sys.stdin as fin:
        b = Basins(fin)
        b.find_basins()
        b.uf.flatten()
        # print b.dump()
        print ' '.join(map(str, b.uf.components_size()))


if __name__ == "__main__":
    main()
