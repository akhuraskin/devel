__author__ = 'ask'

import numpy as np

def simulate(n):
    points = np.sort(np.random.sample(n - 1))
    return np.max(np.diff(np.append(0, np.append(points, 1.)))) > 0.5


T=10000
for n in xrange(2, 10):
    print n, np.mean([simulate(n) for _ in xrange(T)])